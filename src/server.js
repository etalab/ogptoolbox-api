// OGPToolbox-API -- HTTP API for the OGPToolbox database
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-api
//
// OGPToolbox-API is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-API is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import bodyParserFactory from "koa-bodyparser"
import convert from "koa-convert"
import corsFactory from "koa-cors"
import http from "http"
import Koa from "koa"
import routerFactory from "koa-router"
import swaggerValidatorFactory from "koa-swagger"

import config from "./config"
import {checkDatabase} from "./database"
import * as organizationsController from "./controllers/organizations"
import * as platformsController from "./controllers/platforms"
import * as programsController from "./controllers/programs"
import * as swaggerController from "./controllers/swagger"
import * as tagsController from "./controllers/tags"
import * as usagesController from "./controllers/usages"
import * as usersController from "./controllers/users"


checkDatabase()
  .then(startKoa)
  .catch(console.log.bind(console))


let bodyParser = bodyParserFactory()

// Patch Swagger spec, because ko-swagger uses :xxx instead of {xxx} for parameters in path.
// See https://github.com/rricard/koa-swagger/issues/2.
let patchedSwaggerSpec = {...swaggerController.SPEC}
let paths = {}
for (let path in patchedSwaggerSpec.paths) {
  paths[path.replace(/\/\{/g, "/:").replace(/\}/g, "")] = patchedSwaggerSpec.paths[path]
}
patchedSwaggerSpec.paths = paths
let swaggerValidator = convert(swaggerValidatorFactory(patchedSwaggerSpec))

let router = routerFactory()

router.post("/login", bodyParser, swaggerValidator, usersController.login)

router.get("/organizations", swaggerValidator, usersController.authenticate(false),
  organizationsController.listOrganizations)
router.post("/organizations", bodyParser, swaggerValidator, usersController.authenticate(true),
  organizationsController.createOrganization)
router.delete("/organizations/:organizationId", swaggerValidator, usersController.authenticate(true),
  organizationsController.requireOrganization, organizationsController.deleteOrganization)
router.get("/organizations/:organizationId", swaggerValidator, usersController.authenticate(false),
  organizationsController.requireOrganization, organizationsController.getOrganization)
router.put("/organizations/:organizationId", bodyParser, swaggerValidator, usersController.authenticate(true),
  organizationsController.requireOrganization, organizationsController.updateOrganization)

router.get("/platforms", swaggerValidator, usersController.authenticate(false), platformsController.listPlatforms)
router.post("/platforms", bodyParser, swaggerValidator, usersController.authenticate(true),
  platformsController.createPlatform)
router.delete("/platforms/:platformId", swaggerValidator, usersController.authenticate(true),
  platformsController.requirePlatform, platformsController.deletePlatform)
router.get("/platforms/:platformId", swaggerValidator, usersController.authenticate(false),
  platformsController.requirePlatform, platformsController.getPlatform)
router.put("/platforms/:platformId", bodyParser, swaggerValidator, usersController.authenticate(true),
  platformsController.requirePlatform, platformsController.updatePlatform)

router.get("/programs", swaggerValidator, usersController.authenticate(false), programsController.listPrograms)
router.post("/programs", bodyParser, swaggerValidator, usersController.authenticate(true),
  programsController.createProgram)
router.delete("/programs/:programId", swaggerValidator, usersController.authenticate(true),
  programsController.requireProgram, programsController.deleteProgram)
router.get("/programs/:programId", swaggerValidator, usersController.authenticate(false),
  programsController.requireProgram, programsController.getProgram)
router.put("/programs/:programId", bodyParser, swaggerValidator, usersController.authenticate(true),
  programsController.requireProgram, programsController.updateProgram)

router.get("/swagger.json", swaggerController.getSwagger)

router.get("/tags", swaggerValidator, usersController.authenticate(false), tagsController.listTags)
router.post("/tags", bodyParser, swaggerValidator, usersController.authenticate(true),
  tagsController.createTag)
router.delete("/tags/:tagId", swaggerValidator, usersController.authenticate(true),
  tagsController.requireTag, tagsController.deleteTag)
router.get("/tags/:tagId", swaggerValidator, usersController.authenticate(false),
  tagsController.requireTag, tagsController.getTag)
router.put("/tags/:tagId", bodyParser, swaggerValidator, usersController.authenticate(true),
  tagsController.requireTag, tagsController.updateTag)

router.get("/usages", swaggerValidator, usersController.authenticate(false), usagesController.listUsages)
router.post("/usages", bodyParser, swaggerValidator, usersController.authenticate(true),
  usagesController.createUsage)
router.delete("/usages/:usageId", swaggerValidator, usersController.authenticate(true),
  usagesController.requireUsage, usagesController.deleteUsage)
router.get("/usages/:usageId", swaggerValidator, usersController.authenticate(false),
  usagesController.requireUsage, usagesController.getUsage)
router.put("/usages/:usageId", bodyParser, swaggerValidator, usersController.authenticate(true),
  usagesController.requireUsage, usagesController.updateUsage)

router.get("/users", swaggerValidator, usersController.listUsersUrlName)
router.post("/users", bodyParser, swaggerValidator, usersController.createUser)
// router.put("/users", bodyParser, swaggerValidator, usersController.updateUser)
router.delete("/users/:userName", swaggerValidator, usersController.requireUser, usersController.authenticate(true),
  usersController.deleteUser)
router.get("/users/:userName", swaggerValidator, usersController.requireUser, usersController.authenticate(false),
  usersController.getUser)
// router.patch("/users/:userName", swaggerValidator, usersController.requireUser, usersController.patchUser)

let app = new Koa()
app.keys = config.keys
app.name = config.title
app.proxy = config.proxy
app
  .use(async function (ctx, next) {
    // Error handling middleware
    try {
      await next()
    } catch (e) {
      ctx.status = e.status || 500
      ctx.body = {
        apiVersion: "1",
        code: 500,
        message: e.message || http.STATUS_CODES[ctx.status],
      }
      ctx.app.emit("error", e, ctx)
    }
  })
  .use(convert(corsFactory()))
  .use(router.routes())
  .use(router.allowedMethods())


function startKoa() {
  let host = config.listen.host
  let port = config.listen.port || config.port
  app.listen(port, host)
  console.log(`Listening on ${host || "*"}:${port}...`)
}
