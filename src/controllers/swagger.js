// OGPToolbox-API -- HTTP API for the OGPToolbox database
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-api
//
// OGPToolbox-API is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-API is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import config from "../config"


export const SPEC = {
  swagger: "2.0",
  info: {
    title: config.title,
    description: config.description,
    // termsOfService: "http://api.ogptoolbox.org/terms",
    contact: config.contact,
    license: config.license,
    version: "1",
  },
  host: [80, 443].includes(config.port) ? config.host.toString() : `${config.host}:${config.port}`,
  // basePath: "",
  // schemes: ["http", "https", "ws", "wss"],
  consumes: ["application/json"],
  produces: ["application/json"],
  paths: {
    "/login": {
      post: {
        tags: ["user"],
        summary: "Login (to retrieve API key)",
        // description: "",
        // externalDocs: {},
        operationId: "users.login",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            name: "user",
            in: "body",
            // description: "",
            required: true,
            schema: {
              type: "object",
              properties: {
                password: {
                  type: "string",
                },
                userName: {
                  type: "string",
                },
              },
              required: [
                "password",
                "userName",
              ],
            },
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the logged-in user (with its API key)",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/User",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
    },
    "/organizations": {
      get: {
        tags: ["organization"],
        summary: "List organizations",
        // description: "",
        // externalDocs: {},
        operationId: "organizations.list",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/apiKeyOptionalParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing organizations",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  items: {
                    $ref: "#/definitions/Organization",
                  },
                  type: "array",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      post: {
        tags: ["organization"],
        summary: "Create a new organization",
        // description: "",
        // externalDocs: {},
        operationId: "organizations.create",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/organizationParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "201": {
            description: "A wrapper containing the created organization",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Organization",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/organizations/{organizationId}": {
      delete: {
        tags: ["organization"],
        summary: "Delete an existing organization",
        // description: "",
        // externalDocs: {},
        operationId: "organizations.delete",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/organizationIdParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the deleted organization",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Organization",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
      get: {
        tags: ["organization"],
        summary: "Get a organization",
        // description: "",
        // externalDocs: {},
        operationId: "organizations.get",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/organizationIdParam",
          },
          {
            $ref: "#/parameters/apiKeyOptionalParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the requested organization",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Organization",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      put: {
        tags: ["organization"],
        summary: "Update an existing organization",
        // description: "",
        // externalDocs: {},
        operationId: "organizations.update",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/organizationIdParam",
          },
          {
            $ref: "#/parameters/organizationParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the updated organization",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Organization",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/platforms": {
      get: {
        tags: ["platform"],
        summary: "List platforms",
        // description: "",
        // externalDocs: {},
        operationId: "platforms.list",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/apiKeyOptionalParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing platforms",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  items: {
                    $ref: "#/definitions/Platform",
                  },
                  type: "array",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      post: {
        tags: ["platform"],
        summary: "Create a new platform",
        // description: "",
        // externalDocs: {},
        operationId: "platforms.create",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/platformParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "201": {
            description: "A wrapper containing the created platform",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Platform",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/platforms/{platformId}": {
      delete: {
        tags: ["platform"],
        summary: "Delete an existing platform",
        // description: "",
        // externalDocs: {},
        operationId: "platforms.delete",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/platformIdParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the deleted platform",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Platform",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
      get: {
        tags: ["platform"],
        summary: "Get a platform",
        // description: "",
        // externalDocs: {},
        operationId: "platforms.get",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/platformIdParam",
          },
          {
            $ref: "#/parameters/apiKeyOptionalParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the requested platform",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Platform",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      put: {
        tags: ["platform"],
        summary: "Update an existing platform",
        // description: "",
        // externalDocs: {},
        operationId: "platforms.update",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/platformIdParam",
          },
          {
            $ref: "#/parameters/platformParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the updated platform",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Platform",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/programs": {
      get: {
        tags: ["program"],
        summary: "List programs",
        // description: "",
        // externalDocs: {},
        operationId: "programs.list",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/apiKeyOptionalParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing programs",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  items: {
                    $ref: "#/definitions/Program",
                  },
                  type: "array",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      post: {
        tags: ["program"],
        summary: "Create a new program",
        // description: "",
        // externalDocs: {},
        operationId: "programs.create",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/programParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "201": {
            description: "A wrapper containing the created program",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Program",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/programs/{programId}": {
      delete: {
        tags: ["program"],
        summary: "Delete an existing program",
        // description: "",
        // externalDocs: {},
        operationId: "programs.delete",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/programIdParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the deleted program",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Program",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
      get: {
        tags: ["program"],
        summary: "Get a program",
        // description: "",
        // externalDocs: {},
        operationId: "programs.get",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/programIdParam",
          },
          {
            $ref: "#/parameters/apiKeyOptionalParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the requested program",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Program",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      put: {
        tags: ["program"],
        summary: "Update an existing program",
        // description: "",
        // externalDocs: {},
        operationId: "programs.update",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/programIdParam",
          },
          {
            $ref: "#/parameters/programParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the updated program",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Program",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/tags": {
      get: {
        tags: ["tag"],
        summary: "List tags",
        // description: "",
        // externalDocs: {},
        operationId: "tags.list",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/apiKeyOptionalParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing tags",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  items: {
                    $ref: "#/definitions/Tag",
                  },
                  type: "array",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      post: {
        tags: ["tag"],
        summary: "Create a new tag",
        // description: "",
        // externalDocs: {},
        operationId: "tags.create",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/tagParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "201": {
            description: "A wrapper containing the created tag",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Tag",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/tags/{tagId}": {
      delete: {
        tags: ["tag"],
        summary: "Delete an existing tag",
        // description: "",
        // externalDocs: {},
        operationId: "tags.delete",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/tagIdParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the deleted tag",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Tag",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
      get: {
        tags: ["tag"],
        summary: "Get a tag",
        // description: "",
        // externalDocs: {},
        operationId: "tags.get",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/tagIdParam",
          },
          {
            $ref: "#/parameters/apiKeyOptionalParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the requested tag",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Tag",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      put: {
        tags: ["tag"],
        summary: "Update an existing tag",
        // description: "",
        // externalDocs: {},
        operationId: "tags.update",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/tagIdParam",
          },
          {
            $ref: "#/parameters/tagParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the updated tag",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Tag",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/usages": {
      get: {
        tags: ["usage"],
        summary: "List usages",
        // description: "",
        // externalDocs: {},
        operationId: "usages.list",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/apiKeyOptionalParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing usages",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  items: {
                    $ref: "#/definitions/Usage",
                  },
                  type: "array",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      post: {
        tags: ["usage"],
        summary: "Create a new usage",
        // description: "",
        // externalDocs: {},
        operationId: "usages.create",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/usageParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "201": {
            description: "A wrapper containing the created usage",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Usage",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/usages/{usageId}": {
      delete: {
        tags: ["usage"],
        summary: "Delete an existing usage",
        // description: "",
        // externalDocs: {},
        operationId: "usages.delete",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/usageIdParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the deleted usage",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Usage",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
      get: {
        tags: ["usage"],
        summary: "Get a usage",
        // description: "",
        // externalDocs: {},
        operationId: "usages.get",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/usageIdParam",
          },
          {
            $ref: "#/parameters/apiKeyOptionalParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the requested usage",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Usage",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      put: {
        tags: ["usage"],
        summary: "Update an existing usage",
        // description: "",
        // externalDocs: {},
        operationId: "usages.update",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/usageIdParam",
          },
          {
            $ref: "#/parameters/usageParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the updated usage",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Usage",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/users": {
      get: {
        tags: ["user"],
        summary: "List IDs of users",
        // description: "",
        // externalDocs: {},
        operationId: "users.list",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [],
        responses: {
          "200": {
            description: "A wrapper containing references to users",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/UrlName",
                  },
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      post: {
        tags: ["user"],
        summary: "Create a new user",
        // description: "",
        // externalDocs: {},
        operationId: "users.create",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/userParam",
          },
        ],
        responses: {
          "201": {
            description: "A wrapper containing the created user",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/User",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      // put: {
      //   tags: ["user"],
      //   summary: "Update an existing user",
      //   // description: "",
      //   // externalDocs: {},
      //   operationId: "users.update",
      //   // consumes: ["application/json"],
      //   // produces: ["application/json"],
      //   parameters: [
      //     {
      //       $ref: "#/parameters/userParam",
      //     },
      //     {
      //       $ref: "#/parameters/apiKeyRequiredParam",
      //     },
      //   ],
      //   responses: {
      //     "200": {
      //       description: "A wrapper containing the updated user",
      //       schema: {
      //         type: "object",
      //         properties: {
      //           apiVersion: {
      //             type: "string",
      //           },
      //           data: {
      //             $ref: "#/definitions/User",
      //           },
      //         },
      //         required: [
      //           "apiVersion",
      //           "data",
      //         ],
      //       },
      //     },
      //     default: {
      //       description: "Error payload",
      //       schema: {
      //         $ref: "#/definitions/Error",
      //       },
      //     },
      //   },
      //   deprecated: true,
      //   schemes: ["http", "https", "ws", "wss"],
      //   security: [{apiKey: []}, {basic: []}],
      // },
    },
    "/users/{userName}": {
      delete: {
        tags: ["user"],
        summary: "Delete an existing user",
        // description: "",
        // externalDocs: {},
        operationId: "users.delete",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/userNameParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the deleted user",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/User",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
      get: {
        tags: ["user"],
        summary: "Get a user",
        // description: "",
        // externalDocs: {},
        operationId: "users.get",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/userNameParam",
          },
          {
            $ref: "#/parameters/showParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the requested user",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/User",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
    },
    // parameters: {},
  },
  definitions: {
    // Error: {
    //   type: "object",
    //   properties: {
    //     apiVersion: {
    //       type: "string",
    //     },
    //     code: {
    //       type: "integer",
    //       minimum: 100,
    //       maximum: 600,
    //     },
    //     message: {
    //       type: "string",
    //     },
    //   },
    //   required: [
    //     "apiVersion",
    //     "code",
    //     "message",
    //   ],
    // },
    Id: {
      type: "string",
      pattern: "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$",
    },
    Organization: {
      type: "object",
      properties: {
        deleted: {
          type: "boolean",
          default: false,
        },
        name: {
          type: "string",
        },
        updatedAt: {
          type: "string",
          format: "date-time",
        },
        id: {
          $ref: "#/definitions/Id",
        },
      },
      required: [
        "name",
      ],
    },
    Platform: {
      type: "object",
      properties: {
        deleted: {
          type: "boolean",
          default: false,
        },
        name: {
          type: "string",
        },
        updatedAt: {
          type: "string",
          format: "date-time",
        },
        id: {
          $ref: "#/definitions/Id",
        },
      },
      required: [
        "name",
      ],
    },
    Program: {
      type: "object",
      properties: {
        deleted: {
          type: "boolean",
          default: false,
        },
        name: {
          type: "string",
        },
        updatedAt: {
          type: "string",
          format: "date-time",
        },
        id: {
          $ref: "#/definitions/Id",
        },
      },
      required: [
        "name",
      ],
    },
    Tag: {
      type: "object",
      properties: {
        deleted: {
          type: "boolean",
          default: false,
        },
        name: {
          type: "string",
        },
        updatedAt: {
          type: "string",
          format: "date-time",
        },
        id: {
          $ref: "#/definitions/Id",
        },
      },
      required: [
        "name",
      ],
    },
    Usage: {
      type: "object",
      properties: {
        deleted: {
          type: "boolean",
          default: false,
        },
        name: {
          type: "string",
        },
        updatedAt: {
          type: "string",
          format: "date-time",
        },
        id: {
          $ref: "#/definitions/Id",
        },
      },
      required: [
        "name",
      ],
    },
    UrlName: {
      type: "string",
    },
    User: {
      type: "object",
      properties: {
        apiKey: {
          type: "string",
        },
        email: {
          formet: "email",
          type: "string",
        },
        id: {
          $ref: "#/definitions/Id",
        },
        name: {
          type: "string",
        },
        password: {
          type: "string",
        },
        urlName: {
          type: "string",
        },
      },
      required: [
        "email",
        "urlName",
      ],
    },
  },
  parameters: {
    apiKeyOptionalParam: {
      description: "Secret key used to identify user",
      // format: "password",  Don't use password format to allow key to be visible in Swagger-UI.
      in: "header",
      name: "ogptoolbox-api-key",
      type: "string",
    },
    apiKeyRequiredParam: {
      description: "Secret key used to identify user",
      // format: "password",  Don't use password format to allow key to be visible in Swagger-UI.
      in: "header",
      name: "ogptoolbox-api-key",
      required: true,
      type: "string",
    },
    organizationIdParam: {
      // description: "",
      in: "path",
      name: "organizationId",
      required: true,
      // A reference to a non-object definition doesn't work for a parameter that is not in request body.
      // schema: {
      //   $ref: "#/definitions/Id",
      // },
      type: "string",
      pattern: "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$",
    },
    organizationParam: {
      // description: "",
      in: "body",
      name: "organization",
      required: true,
      schema: {
        $ref: "#/definitions/Organization",
      },
    },
    platformIdParam: {
      // description: "",
      in: "path",
      name: "platformId",
      required: true,
      // A reference to a non-object definition doesn't work for a parameter that is not in request body.
      // schema: {
      //   $ref: "#/definitions/Id",
      // },
      type: "string",
      pattern: "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$",
    },
    platformParam: {
      // description: "",
      in: "body",
      name: "platform",
      required: true,
      schema: {
        $ref: "#/definitions/Platform",
      },
    },
    programIdParam: {
      // description: "",
      in: "path",
      name: "programId",
      required: true,
      // A reference to a non-object definition doesn't work for a parameter that is not in request body.
      // schema: {
      //   $ref: "#/definitions/Id",
      // },
      type: "string",
      pattern: "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$",
    },
    programParam: {
      // description: "",
      in: "body",
      name: "program",
      required: true,
      schema: {
        $ref: "#/definitions/Program",
      },
    },
    tagIdParam: {
      // description: "",
      in: "path",
      name: "tagId",
      required: true,
      // A reference to a non-object definition doesn't work for a parameter that is not in request body.
      // schema: {
      //   $ref: "#/definitions/Id",
      // },
      type: "string",
      pattern: "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$",
    },
    tagParam: {
      // description: "",
      in: "body",
      name: "tag",
      required: true,
      schema: {
        $ref: "#/definitions/Tag",
      },
    },
    usageIdParam: {
      // description: "",
      in: "path",
      name: "usageId",
      required: true,
      // A reference to a non-object definition doesn't work for a parameter that is not in request body.
      // schema: {
      //   $ref: "#/definitions/Id",
      // },
      type: "string",
      pattern: "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$",
    },
    usageParam: {
      // description: "",
      in: "body",
      name: "usage",
      required: true,
      schema: {
        $ref: "#/definitions/Usage",
      },
    },
    userNameParam: {
      // description: "",
      in: "path",
      name: "userName",
      required: true,
      type: "string",
    },
    userParam: {
      // description: "",
      in: "body",
      name: "user",
      required: true,
      schema: {
        $ref: "#/definitions/User",
      },
    },
  },
  // externalDocs: {},
  // responses: {},
  // security: {},
  // securityDefinitions: {
  //   apiKey: {
  //     description: "Secret key used to identify user or bot",
  //     in: "header",
  //     name: "ogptoolbox-api-key",
  //     type: "apiKey",
  //   },
  //   basic: {
  //     description: "HTTP Basic authentication with user (or bot) name and password",
  //     type: "basic",
  //   },
  // },
  // tags: [],
}


export {getSwagger}
async function getSwagger(ctx){
  ctx.body = JSON.stringify(SPEC)
}
