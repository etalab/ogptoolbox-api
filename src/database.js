// OGPToolbox-API -- HTTP API for the OGPToolbox database
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-api
//
// OGPToolbox-API is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-API is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import assert from "assert"
import rethinkdbdashFactory from "rethinkdbdash"

import config from "./config"


export const r = rethinkdbdashFactory({
    db: config.db.name,
    host: config.db.host,
    port: config.db.port,
  })

const versionNumber = 4


export {checkDatabase}
async function checkDatabase() {
  const databasesName = await r.dbList()
  assert(databasesName.includes(config.db.name), 'Database is not initialized. Run "node configure" to configure it.')
  let versions
  try {
    versions = await r.table("version")
  } catch (e) {
    throw new Error('Database is not initialized. Run "node configure" to configure it.')
  }
  assert(versions.length > 0, 'Database is not initialized. Run "node configure" to configure it.')
  assert.strictEqual(versions.length, 1)
  const version = versions[0]
  assert(version.number <= versionNumber, "Database format is too recent. Upgrade OGPToolbox-API.")
  assert.strictEqual(version.number, versionNumber, 'Database must be upgraded. Run "node configure" to configure it.')
}


export {configure}
async function configure() {
  const databasesName = await r.dbList()
  if (!databasesName.includes(config.db.name)) {
    await r.dbCreate(config.db.name)
    await r.tableCreate("version")
    await r.table("version").insert({number: versionNumber})
  }

  try {
    await r.table("organizations").count()
  } catch (e) {
    await r.tableCreate("organizations")
  }
  const organizationsTable = r.table("organizations")
  try {
    await organizationsTable.indexWait("name")
  } catch (e) {
    await organizationsTable.indexCreate("name")
  }
  try {
    await organizationsTable.indexWait("updatedAt")
  } catch (e) {
    await organizationsTable.indexCreate("updatedAt")
  }

  try {
    await r.table("platforms").count()
  } catch (e) {
    await r.tableCreate("platforms")
  }
  const platformsTable = r.table("platforms")
  try {
    await platformsTable.indexWait("name")
  } catch (e) {
    await platformsTable.indexCreate("name")
  }
  try {
    await platformsTable.indexWait("updatedAt")
  } catch (e) {
    await platformsTable.indexCreate("updatedAt")
  }

  try {
    await r.table("programs").count()
  } catch (e) {
    await r.tableCreate("programs")
  }
  const programsTable = r.table("programs")
  try {
    await programsTable.indexWait("name")
  } catch (e) {
    await programsTable.indexCreate("name")
  }
  try {
    await programsTable.indexWait("updatedAt")
  } catch (e) {
    await programsTable.indexCreate("updatedAt")
  }

  try {
    await r.table("tags").count()
  } catch (e) {
    await r.tableCreate("tags")
  }
  const tagsTable = r.table("tags")
  try {
    await tagsTable.indexWait("name")
  } catch (e) {
    await tagsTable.indexCreate("name")
  }
  try {
    await tagsTable.indexWait("updatedAt")
  } catch (e) {
    await tagsTable.indexCreate("updatedAt")
  }

  try {
    await r.table("usages").count()
  } catch (e) {
    await r.tableCreate("usages")
  }
  const usagesTable = r.table("usages")
  try {
    await usagesTable.indexWait("name")
  } catch (e) {
    await usagesTable.indexCreate("name")
  }
  try {
    await usagesTable.indexWait("updatedAt")
  } catch (e) {
    await usagesTable.indexCreate("updatedAt")
  }

  try {
    await r.table("users").count()
  } catch (e) {
    await r.tableCreate("users")
  }
  const usersTable = r.table("users")
  try {
    await usersTable.indexWait("apiKey")
  } catch (e) {
    await usersTable.indexCreate("apiKey")
  }
  try {
    await usersTable.indexWait("createdAt")
  } catch (e) {
    await usersTable.indexCreate("createdAt")
  }
  try {
    await usersTable.indexWait("email")
  } catch (e) {
    await usersTable.indexCreate("email")
  }
  try {
    await usersTable.indexWait("urlName")
  } catch (e) {
    await usersTable.indexCreate("urlName")
  }

  try {
    await r.table("version").count()
  } catch (e) {
    await r.tableCreate("version")
  }
  const versionTable = r.table("version")
  const versions = await versionTable
  let version
  if (versions.length === 0) {
    let result = await versionTable.insert({number: 0}, {returnChanges: true})
    version = result.changes[0].new_val
  } else {
    assert.strictEqual(versions.length, 1)
    version = versions[0]
  }
  assert(version.number <= versionNumber, "Database format is too recent. Upgrade OGPToolbox-API.")

  const previousVersionNumber = version.number

  if (version.number === 0) version.number += 1
  if (version.number === 1) version.number += 1
  if (version.number === 2) {
    await r.table("methods").filter(r.row("description")).replace(function (entry) {
      return entry.without("description").merge({"description_fr": entry("description")})
    })

    await r.table("projects").filter(r.row("description")).replace(function (entry) {
      return entry.without("description").merge({"description_fr": entry("description")})
    })

    await r.table("tools").filter(r.row("categories")).replace(function (entry) {
      return entry.without("categories").merge({"otherCategories": entry("categories")})
    })
    await r.table("tools").filter(r.row("description")).replace(function (entry) {
      return entry.without("description").merge({"description_fr": entry("description")})
    })

    version.number += 1
  }
  if (version.number === 3) {
    await r.tableDrop("methods")
    await r.tableDrop("projects")
    await r.tableDrop("tools")

    version.number += 1
  }

  assert(version.number <= versionNumber,
    `Error in database upgrade script: Wrong version number: ${version.number} > ${versionNumber}.`)
  assert(version.number >= previousVersionNumber,
    `Error in database upgrade script: Wrong version number: ${version.number} < ${previousVersionNumber}.`)
  if (version.number !== previousVersionNumber) await versionTable.get(version.id).update({number: version.number})
}
